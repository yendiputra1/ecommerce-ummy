<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\PelangganController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Route::resource('pelanggan', PelangganController::class);
Route::resource('products', ProductController::class);
// Route::get('/update_password', [HomeController::class, 'update_password'])->name('update_password');
// Route::patch('/store_password', [HomeController::class, 'store_password'])->name('store_password');

Route::middleware(['admin'])->group(function () {
    Route::get('/update_password', [HomeController::class, 'update_password'])->name('update_password');
    Route::patch('/store_password', [HomeController::class, 'store_password'])->name('store_password');

    Route::resource('products', ProductController::class);
});
Route::middleware(['user'])->group(function () {
    Route::resource('products', ProductController::class);
});
Route::middleware(['operator'])->group(function () {
    Route::resource('products', ProductController::class);
});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
